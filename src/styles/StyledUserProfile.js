import styled from 'styled-components'

const StyledUserProfile = styled.div`
  display: flex;
  @media screen and (max-width: 767px) {
    flex-direction: column;
  }
`

export default StyledUserProfile
