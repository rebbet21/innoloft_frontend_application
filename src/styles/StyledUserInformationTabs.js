import styled from 'styled-components'

const StyledUserInformationTabs = styled.div`
  display: flex;

  & {
    div {
      padding: 1rem;
      cursor: pointer;
      flex: 1;
      text-align: center;

      font-size: 1.2rem;

      &:hover {
        background-color: #efefef;
      }
      &.active {
        font-weight: bold;
        background-color: #dae160;
      }
    }

    @media screen and (max-width: 767px) {
      flex-direction: column;
    }
  }
`

export default StyledUserInformationTabs
