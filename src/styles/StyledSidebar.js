import styled from 'styled-components'

const StyledSidebar = styled.div`
  display: flex;
  flex-direction: column;
  font-size: 0.9rem;
  gap: 0.5rem;

  @media screen and (max-width: 767px) {
    margin-bottom: 2rem;
  }

  div {
    width: 80%;
    color: darkslategrey;
    padding: 0.8rem;
    transition: all 200ms ease-in;
    cursor: pointer;
    &:hover {
      background-color: lightgray;
    }
    svg {
      height: 1rem;
      width: 1rem;
      fill: darkslategrey;
    }
  }
`

export default StyledSidebar
