import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import StyledUserInformationTabs from '../styles/StyledUserInformationTabs'
import StyledUserInformation from '../styles/StyledUserInformation'
import StyledAlert from '../styles/StyledAlert'
import MainTab from './MainTab'
import AdditionalInfoTab from './AdditionalInfoTab'
import { setActiveTab } from '../store/actions'

const UserInformation = () => {
  const activeTab = useSelector((state) => state.activeTab)
  const dispatch = useDispatch()

  const appState = useSelector((state) => state)
  const setTab = (tabId) => {
    dispatch(setActiveTab(tabId))
  }
  return (
    <StyledUserInformation>
      <StyledUserInformationTabs>
        <div
          tabIndex={0}
          className={activeTab === 0 ? 'active' : ''}
          role="button"
          onClick={() => setTab(0)}
          onKeyDown={() => setTab(0)}
        >
          Main Information
        </div>
        <div
          tabIndex={0}
          className={activeTab === 1 ? 'active' : ''}
          role="button"
          onClick={() => setTab(1)}
          onKeyDown={() => setTab(1)}
        >
          {' '}
          Additional Information
        </div>
      </StyledUserInformationTabs>
      {appState.accountSaved ? (
        <StyledAlert>
          <span>User Main Account Information Successfully Saved!</span>
        </StyledAlert>
      ) : (
        <div />
      )}
      {appState.userInfoSaved ? (
        <StyledAlert>
          <span>User Additional Information Successfully Saved!</span>
        </StyledAlert>
      ) : (
        <div />
      )}
      {activeTab === 0 ? <MainTab /> : <AdditionalInfoTab />}
    </StyledUserInformation>
  )
}

export default UserInformation
