import React from 'react'
import StyledUserPage from './styles/StyledUserPage'
import Header from './components/Header'
import Sidebar from './components/Sidebar'
import UserInformation from './components/UserInformation'
import StyledUserProfile from './styles/StyledUserProfile'
import StyledSizedBox from './styles/StyledSizedBox'
import StyledContainer from './styles/StyledContainer'

function App() {
  return (
    <StyledUserPage>
      <Header />
      <StyledSizedBox height="8" />

      <StyledUserProfile>
        <StyledContainer flex="1" />
        <StyledContainer flex="1">
          <Sidebar />
        </StyledContainer>
        <StyledContainer flex="2">
          <UserInformation />
        </StyledContainer>
        <StyledContainer flex="1" />
      </StyledUserProfile>
    </StyledUserPage>
  )
}

export default App
