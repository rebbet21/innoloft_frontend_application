import styled from 'styled-components'

const StyledFlex = styled.div`
  display: flex;
  flex-direction: ${(props) => props.direction || 'row'};
  align-items: ${(props) => props.align_items || 'normal'};
  gap: ${(props) => `${props.gap}rem` || 0};
`

export default StyledFlex
