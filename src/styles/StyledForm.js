import styled from 'styled-components'

const StyledForm = styled.form`
  display: flex;
  flex-direction: column;
  align-items: start;
  gap: 2rem;
  padding: 4rem 3rem;
  div {
    display: flex;
    width: 60%;
    flex-direction: column;
    gap: 0.5rem;
    align-items: start;
    label {
      color: darkslategrey;

      font-size: 1.1rem;
    }
    input {
      padding: 0.8rem 1rem;
      width: 100%;
      border: 1px solid lightgray;
    }
    button {
      padding: 1rem 2rem;
      text-transform: uppercase;
      background-color: #649961;
      color: white;
      transition: all 200ms ease-in;
      border: none;
      font-size: 1.05rem;
      border-radius: 0.5rem;

      &:hover {
        background-color: #317b62;
      }
    }
  }
`

export default StyledForm
