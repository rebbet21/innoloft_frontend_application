import { createStore } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import userReducer from './reducers'

const enhancer = composeWithDevTools()
const store = createStore(userReducer, enhancer)

export default store
