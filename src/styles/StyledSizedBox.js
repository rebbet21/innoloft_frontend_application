import styled from 'styled-components'

const StyledSizedBox = styled.div`
  width: ${(props) => `${props.width}rem` || 0};
  height: ${(props) => `${props.height}rem` || 0};
`

export default StyledSizedBox
