import React, { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import {
  savingUserInfo,
  saveUserInfo,
  resetUserInfoSaved,
} from '../store/actions'
import StyledForm from '../styles/StyledForm'

const AdditionalInfoTab = () => {
  // internal state
  const [firstName, setFirstName] = useState('')
  const [lastName, setLastName] = useState('')
  const [address, setAddress] = useState('')
  const [country, setCountry] = useState('')

  // redux state
  const dispatch = useDispatch()
  const userInfoSaving = useSelector((state) => state.userInfoSaving)

  const handleSubmit = (e) => {
    e.preventDefault()

    if (
      firstName.length &&
      lastName.length &&
      address.length &&
      country.length
    ) {
      // imitate an ajax call
      dispatch(savingUserInfo())
      setTimeout(() => {
        dispatch(saveUserInfo(firstName, lastName, address, country))
        // reset fields
        setFirstName('')
        setLastName('')
        setAddress('')
        setCountry('')
        setTimeout(() => {
          dispatch(resetUserInfoSaved())
        }, 1500)
      }, 1000)
    }
  }

  return (
    <StyledForm onSubmit={(e) => handleSubmit(e)}>
      <div>
        <label htmlFor="first-name"> First Name</label>
        <input
          type="text"
          id="first-name"
          value={firstName}
          onChange={(e) => setFirstName(e.target.value)}
          required
        />
      </div>
      <div>
        <label htmlFor="last-name">Last Name</label>
        <input
          type="text"
          id="last-name"
          value={lastName}
          onChange={(e) => setLastName(e.target.value)}
          required
        />
      </div>
      <div>
        {' '}
        <label htmlFor="address">Address</label>
        <input
          type="text"
          id="address"
          value={address}
          onChange={(e) => setAddress(e.target.value)}
          required
        />
      </div>
      <div>
        <label htmlFor="country">Country</label>
        <input
          type="text"
          id="country"
          value={country}
          onChange={(e) => setCountry(e.target.value)}
          required
        />
      </div>

      <div>
        <button type="submit">{userInfoSaving ? 'updating' : 'update'}</button>
      </div>
    </StyledForm>
  )
}

export default AdditionalInfoTab
