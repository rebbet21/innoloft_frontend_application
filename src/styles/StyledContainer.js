import styled from 'styled-components'

const StyledContainer = styled.div`
  flex: ${(props) => props.flex || 'none'};
`

export default StyledContainer
