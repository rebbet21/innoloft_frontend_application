import { ACTIONS } from './actions'

const userReducer = (
  state = {
    account: {},
    userInfo: {},
    activeTab: 0,
    accountSaved: false,
    userInfoSaved: false,
    accountSettingSaving: false,
    userInfoSaving: false,
  },
  action,
) => {
  switch (action.type) {
    case ACTIONS.SAVE_ACCOUNT_SETTING:
      return {
        ...state,
        account: action.payload,
        accountSaved: true,
        accountSettingSaving: false,
      }
    case ACTIONS.SAVE_USER_INFO:
      return {
        ...state,
        userInfo: action.payload,
        userInfoSaved: true,
        userInfoSaving: false,
      }

    case ACTIONS.SAVING_ACCOUNT_SETTING:
      return { ...state, accountSettingSaving: true, accountSaved: false }
    case ACTIONS.SAVING_USER_INFO:
      return { ...state, userInfoSaving: true, userInfoSaved: false }

    case ACTIONS.SET_ACTIVE_TAB:
      return { ...state, activeTab: action.payload }

    case ACTIONS.RESET_ACCOUNT_SAVED:
      return { ...state, accountSaved: false }
    case ACTIONS.RESET_USER_INFO_SAVED:
      return { ...state, userInfoSaved: false }

    default:
      return state
  }
}

export default userReducer
