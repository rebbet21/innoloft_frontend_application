import styled from 'styled-components'

const StyledHeader = styled.div`
  display: flex;
  align-items: center;
  background: linear-gradient(90deg, #dae160, #2e7962);
  padding: 1rem;

  @media screen and (max-width: 767px) {
    flex-direction: column;
    align-items: center;
    padding: 0.5rem;
    .language,
    .messages,
    .notifications {
      margin: 0.3rem 0;
    }
  }
  & {
    .language,
    .messages,
    .notifications {
      color: #ececec;
      display: flex;
      flex-direction: row;
      width: 5rem;
      align-items: center;

      svg {
        fill: #ececec;
        height: 1.5rem;
        width: 1.5rem;
      }
    }
    .language {
      width: 7rem;
    }
    .languageCode {
      margin-left: 0.5rem;
      margin-right: 1rem;
    }
    .indicator {
      display: block;
      position: relative;
      top: 0.6rem;
      left: -0.5rem;
      width: 0.8rem;
      height: 0.8rem;
      border-radius: 0.5rem;
      background-color: #c8b24a;
    }
  }
`

export default StyledHeader
