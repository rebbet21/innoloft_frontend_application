import styled from 'styled-components'

const StyledUserInformation = styled.div`
  display: flex;
  flex-direction: column;
  background-color: white;
  box-box-shadow: 1px 1px 6px 0 black;
  border-radius: 1rem;
`

export default StyledUserInformation
