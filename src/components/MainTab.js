import React, { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import {
  resetAccountSaved,
  saveAccountSetting,
  savingAccountSetting,
} from '../store/actions'
import StyledForm from '../styles/StyledForm'

const MainTab = () => {
  // internal states
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [repeatPassword, setRepeatPassword] = useState('')
  // redux state
  const dispatch = useDispatch()
  const accountSettingSaving = useSelector(
    (state) => state.accountSettingSaving,
  )

  const handleSubmit = (e) => {
    e.preventDefault()
    // validate fields
    if (email.length && password.length && repeatPassword.length) {
      if (password === repeatPassword) {
        // imitate an ajax call
        dispatch(savingAccountSetting())
        setTimeout(() => {
          dispatch(saveAccountSetting(email, password, repeatPassword))
          // reset fields
          setEmail('')
          setPassword('')
          setRepeatPassword('')
          setTimeout(() => {
            dispatch(resetAccountSaved())
          }, 1500)
        }, 1000)
      }
    }
  }

  return (
    <StyledForm onSubmit={(e) => handleSubmit(e)}>
      <div>
        <label htmlFor="email">Email Address</label>
        <input
          type="email"
          id="email"
          value={email}
          required
          onChange={(e) => setEmail(e.target.value)}
        />
      </div>

      <div>
        <label htmlFor="password">Password</label>
        <input
          type="password"
          id="password"
          value={password}
          required
          onChange={(e) => setPassword(e.target.value)}
        />
      </div>

      <div>
        <label htmlFor="repeat_password">Repeat Password</label>
        <input
          type="password"
          id="repeat_password"
          value={repeatPassword}
          required
          onChange={(e) => setRepeatPassword(e.target.value)}
        />
      </div>

      <div>
        <button type="submit">
          {accountSettingSaving ? 'updating' : 'update'}
        </button>
      </div>
    </StyledForm>
  )
}

export default MainTab
