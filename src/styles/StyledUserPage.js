import styled from 'styled-components'

const StyledUserPage = styled.div`
  display: flex;
  flex-direction: column;
  background-color: #f6f6f6;
  .expanded {
    flex: 1;
  }
`

export default StyledUserPage
