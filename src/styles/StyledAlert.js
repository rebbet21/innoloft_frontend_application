import styled from 'styled-components'

const StyledAlert = styled.div`
  padding: 1rem;
  background-color: #78a662;
  color: black;
  margin: 3rem 2rem 0 2rem;
  border-radius: 0.5rem;
`

export default StyledAlert
