const ACTIONS = {
  SAVE_ACCOUNT_SETTING: 'SAVE_ACCOUNT_SETTING',
  SAVE_USER_INFO: 'SAVE_USER_INFO',
  SAVING_ACCOUNT_SETTING: 'SAVING_ACCOUNT_SETTING',
  SAVING_USER_INFO: 'SAVING_USER_INFO',
  SET_ACTIVE_TAB: 'SET_ACTIVE_TAB',
  RESET_ACCOUNT_SAVED: 'RESET_ACCOUNT_SAVED',
  RESET_USER_INFO_SAVED: 'RESET_USER_INFO_SAVED',
}

const saveAccountSetting = (email, password, passwordRepeat) => {
  return {
    type: ACTIONS.SAVE_ACCOUNT_SETTING,
    payload: { email, password, passwordRepeat },
  }
}

const saveUserInfo = (firstName, lastName, address, country) => {
  return {
    type: ACTIONS.SAVE_USER_INFO,
    payload: { firstName, lastName, address, country },
  }
}

const savingAccountSetting = () => {
  return { type: ACTIONS.SAVING_ACCOUNT_SETTING }
}

const savingUserInfo = () => {
  return { type: ACTIONS.SAVING_USER_INFO }
}
const setActiveTab = (tabId) => {
  return { type: ACTIONS.SET_ACTIVE_TAB, payload: tabId }
}

const resetAccountSaved = () => {
  return { type: ACTIONS.RESET_ACCOUNT_SAVED }
}
const resetUserInfoSaved = () => {
  return { type: ACTIONS.RESET_USER_INFO_SAVED }
}

export {
  ACTIONS,
  saveAccountSetting,
  saveUserInfo,
  savingAccountSetting,
  savingUserInfo,
  setActiveTab,
  resetAccountSaved,
  resetUserInfoSaved,
}
